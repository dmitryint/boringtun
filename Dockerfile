FROM rust:1 as build

ARG BORINGTUN_VERSION=f824287

RUN export target=`uname -m`-unknown-linux-musl \
 && rustup target install $target \
 && apt update && apt install -y musl-tools \
 && cargo install boringtun --git='https://github.com/cloudflare/boringtun.git' --rev=${BORINGTUN_VERSION} --target=$target \
 && rm -rf /usr/local/cargo/registry 


FROM debian:buster

ENV DEBIAN_FRONTEND="noninteractive"

RUN echo "deb http://deb.debian.org/debian/ unstable main" > /etc/apt/sources.list.d/unstable-wireguard.list && \
    printf 'Package: *\nPin: release a=unstable\nPin-Priority: 90\n' > /etc/apt/preferences.d/limit-unstable && \
    apt-get update && \
    apt-get install --yes --no-install-recommends \
    gnupg iproute2 iptables ifupdown iputils-ping binutils wireguard && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

COPY --from=build /usr/local/cargo/bin/boringtun /usr/local/bin/boringtun
COPY docker-entrypoint.sh /bin/docker-entrypoint.sh
RUN chmod +x /bin/docker-entrypoint.sh

VOLUME ["/var/run/wireguard/"]
CMD [ "run-server" ]
ENTRYPOINT [ "docker-entrypoint.sh" ]
