#!/bin/bash
set -e

if [ "$1" = "run-server" ]; then
    
    /usr/local/bin/boringtun wg0 --log=/dev/stdout --err=/dev/stderr --threads 4 --disable-drop-privileges root

    config=$(ls /etc/wireguard/*.conf | head -1)
    if ip a | grep -q $(basename $config | cut -f 1 -d '.'); then
        echo "Stopping existing interface"
        wg-quick down $config
    fi

    echo "Starting wireguard using $config"
    wg-quick up $config
    echo "Running config:"
    wg

    shutdown() {
        echo "Stopping wireguard"
        wg-quick down $config
        exit 0
    }
    trap shutdown SIGINT SIGTERM SIGQUIT

    sleep infinity &
    wait

elif [ "$1" = "gen-key" ]; then
    PRIVATE_KEY=$(wg genkey)
    PUBLIC_KEY=$(echo ${PRIVATE_KEY} | wg pubkey)
    echo "Private key: ${PRIVATE_KEY}"
    echo "Public key: ${PUBLIC_KEY}"
    exit 0
else
    exec "$@"
fi
